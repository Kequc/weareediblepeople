'use strict';
const express = require('express');

const router = express.Router();

router.get('/', (req, res, next) => {
    const MAPS_API = process.env.MAPS_API;
    res.render('index', { title: 'Weareediblepeople', MAPS_API });
});

module.exports = router;
