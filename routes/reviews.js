'use strict';
const express = require('express');
const Review = require('../models/review');

const router = express.Router();

router.delete('/:id', (req, res, next) => {
    if (res.locals.user === undefined) {
        const err = new Error('Must be signed in.');
        err.status = 403;
        next(err);
    }
    else {
        Review.read(req.params.id, (err, doc) => {
            if (err)
                next(err);
            else if (doc.body.userId !== res.locals.user.id) {
                const err = new Error('Not authorised.');
                err.status = 403;
                next(err);
            }
            else {
                doc.destroy((err) => {
                    if (err)
                        next(err);
                    else
                        res.json({ ok: true });
                });
            }
        });
    }
});

module.exports = router;
