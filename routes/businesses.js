'use strict';
const express = require('express');
const Review = require('../models/review');
const Yelp = require('../utils/yelp');

const router = express.Router();

router.get('/', (req, res, next) => {
    Yelp.query(req.query, (err, businesses) => {
        if (err)
            next(err);
        else
            res.json(businesses);
    });
});

router.get('/:id/reviews', (req, res, next) => {
    Review.findByBusinessId(req.params.id, (err, docs) => {
        if (err)
            next(err);
        else
            res.json(docs.map(doc => doc.toJson()));
    });
});

router.post('/:id/review', (req, res, next) => {
    if (res.locals.user === undefined) {
        const err = new Error('Must be signed in.');
        err.status = 403;
        next(err);
    }
    else {
        const body = {
            businessId: req.params.id,
            userId: res.locals.user.id,
            username: res.locals.user.body.username,
            description: req.body.description,
            createdAt: Date.now()
        };
        if (!body.businessId || !body.description) {
            const err = new Error('Description required.');
            err.status = 400;
            next(err);
        }
        else {
            Review.create(body, (err, doc) => {
                if (err)
                    next(err);
                else
                    res.json(doc.toJson());
            });
        }
    }
});

router.delete('/:id/review', (req, res, next) => {
    if (res.locals.user === undefined) {
        const err = new Error('Must be signed in.');
        err.status = 403;
        next(err);
    }
    else {
        Review.read(req.params.id, (err, doc) => {
            if (err)
                next(err);
            else if (doc.body.userId !== res.locals.user.id) {
                const err = new err('Not authorised.');
                err.status = 403;
                next(err);
            }
            else {
                doc.destroy((err) => {
                    if (err)
                        next(err);
                    else
                        res.json({ ok: true });
                });
            }
        });
    }
});

module.exports = router;
