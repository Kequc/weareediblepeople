'use strict';
const express = require('express');
const jwt = require('jwt-simple');
const User = require('../models/user');

const router = express.Router();

router.post('/', (req, res, next) => {
    const body = {
        username: req.body.username,
        password: req.body.password
    };
    if (!body.username || !body.password) {
        const err = new Error('Username and password required.');
        err.status = 400;
        next(err);
    }
    else {
        User.create(body, (err, doc) => {
            if (err)
                next(err);
            else
                _login(res, doc);
        });
    }
});

router.post('/login', (req, res, next) => {
    User.authenticate(req.body.username, req.body.password, (err, doc) => {
        if (err)
            next(err);
        else
            _login(res, doc);
    });
});

function _login(res, doc) {
    const authtoken = jwt.encode({ id: doc.id, username: doc.body.username }, process.env.JWT_SECRET);
    const options = {
        maxAge: 365 * 24 * 60 * 60 * 1000
    };
    res.cookie('userData', authtoken, options);
    res.json({ ok: true });
}

router.get('/logout', (req, res, next) => {
    res.clearCookie('userData');
    res.json({ ok: true });
});

module.exports = router;
