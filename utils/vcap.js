'use strict';

var Vcap = {
    services: process.env.VCAP_SERVICES ? JSON.parse(process.env.VCAP_SERVICES) : {}
};

Vcap.service = (name, index) => {
    return Vcap.services[name] ? Vcap.services[name][index || 0] : undefined;
};

Vcap.serviceCredentials = (name, index) => {
    let ser = Vcap.service(name, index);
    return ser ? ser.credentials : undefined;
};

module.exports = Vcap;
