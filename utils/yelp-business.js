'use strict';

const YelpBusiness = {};

YelpBusiness.simpleMap = (items) => {
    const result = {};
    for (const data of items) {
        result[data.id] = YelpBusiness.simple(data);
    }
    return result;
};

YelpBusiness.simple = (body) => {
    return {
        name: body.name,
        distance: body.distance,
        phone: body.display_phone,
        rating: body.rating,
        position: {
            lat: (body.coordinates || {}).latitude,
            lng: (body.coordinates || {}).longitude,
        },
        tags: (body.categories || []).map(category => category.title)
    };
};

module.exports = YelpBusiness;
