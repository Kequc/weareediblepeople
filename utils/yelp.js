'use strict';
const request = require('request');
const YelpBusiness = require('./yelp-business');

// Yelp API reference
// https://www.yelp.com/developers/documentation/v3/business_search

const Yelp = {};

Yelp.MAX_TRIES = 5;

Yelp.opt = (opt) => {
    const result = {
        url: 'https://api.yelp.com' + opt.path,
        method: opt.method || 'GET',
        json: true,
        headers: {},
        qs: opt.qs
    };
    if (opt.method === 'POST')
        result.headers['content-type'] = 'application/x-www-form-urlencoded';
    if (opt.auth === true)
        result.headers['Authorization'] = 'Bearer ' + Yelp.ACCESS_TOKEN;
    return result;
};

Yelp.response = (callback) => (error, res, body = {}) => {
    if (!error && body.error !== undefined) {
        error = new Error(body.error.description);
        error.name = body.error.code;
    }
    callback(error, body);
};

Yelp.getAccessToken = (callback) => {
    const opt = Yelp.opt({
        path: '/oauth2/token',
        method: 'POST',
        qs: {
            grant_type: 'client_credentials',
            client_id: process.env.YELP_ID,
            client_secret: process.env.YELP_SECRET
        }
    });
    request(opt, Yelp.response((error, body) => {
        Yelp.ACCESS_TOKEN = body.access_token;
        callback(error, Yelp.ACCESS_TOKEN);
    }));
};

Yelp.query = (params, callback, tries = 0) => {
    tries++;
    const opt = Yelp.opt({
        path: '/v3/businesses/search',
        auth: true,
        qs: {
            latitude: params.lat,
            longitude: params.lng,
            radius: Math.floor(params.radius),
            limit: 26,
            open_now: true,
            term: params.term
        }
    });
    request(opt, Yelp.response((error, body) => {
        if (error && tries <= Yelp.MAX_TRIES && error.name === 'TOKEN_INVALID') {
            Yelp.getAccessToken((error) => {
                if (error)
                    callback(error);
                else
                    Yelp.query(params, callback, tries);
            });
        }
        else
            callback(error, YelpBusiness.simpleMap(body.businesses || []));
    }));
};

module.exports = Yelp;
