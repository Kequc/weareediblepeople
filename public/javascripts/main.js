var Weareediblepeople;
(function (Weareediblepeople) {
    var BusinessList;
    (function (BusinessList) {
        function _onDOMContentLoaded() {
            BusinessList.element = document.getElementById('business-list');
            BusinessList.element.addEventListener('mouseleave', deselectBusiness);
        }
        var _businesses = {};
        var _visible = [];
        var _selected;
        function findBusiness(id) {
            return _businesses[id];
        }
        BusinessList.findBusiness = findBusiness;
        function findBusinessId(business) {
            for (var _i = 0, _a = Object.keys(_businesses); _i < _a.length; _i++) {
                var id = _a[_i];
                if (_businesses[id] === business)
                    return id;
            }
        }
        BusinessList.findBusinessId = findBusinessId;
        function toggleBusinessSelected(business) {
            var id = findBusinessId(business);
            if (_selected !== id)
                selectBusiness(id);
            else
                deselectBusiness();
        }
        BusinessList.toggleBusinessSelected = toggleBusinessSelected;
        function selectBusiness(id) {
            deselectBusiness();
            _businesses[id].marker.setAnimation(google.maps.Animation.BOUNCE);
            _selected = id;
        }
        BusinessList.selectBusiness = selectBusiness;
        function deselectBusiness() {
            if (_selected === undefined)
                return;
            _businesses[_selected].marker.setAnimation(null);
            _selected = undefined;
        }
        BusinessList.deselectBusiness = deselectBusiness;
        function _storeBusinesses(items) {
            for (var _i = 0, _a = Object.keys(items); _i < _a.length; _i++) {
                var id = _a[_i];
                var business = _businesses[id];
                if (business !== undefined)
                    business.update(items[id]);
                else
                    _businesses[id] = new Weareediblepeople.Business(items[id]);
            }
        }
        function _forViewport(id) {
            var bounds = Weareediblepeople.MapWrapper.map.getBounds();
            return bounds.contains(_businesses[id].data.position);
        }
        function _render(ids) {
            ids = ids.filter(_forViewport);
            for (var _i = 0, _visible_1 = _visible; _i < _visible_1.length; _i++) {
                var id = _visible_1[_i];
                if (ids.indexOf(id) <= -1) {
                    BusinessList.element.removeChild(_businesses[id].element);
                    _businesses[id].marker.setMap(null);
                }
            }
            for (var _a = 0, ids_1 = ids; _a < ids_1.length; _a++) {
                var id = ids_1[_a];
                BusinessList.element.appendChild(_businesses[id].element);
                if (_visible.indexOf(id) <= -1)
                    _businesses[id].marker.setMap(Weareediblepeople.MapWrapper.map);
            }
            if (ids.indexOf(_selected) <= -1)
                deselectBusiness();
            _visible = ids;
        }
        function update(items) {
            _storeBusinesses(items);
            var ids = Object.keys(items);
            _render(ids);
        }
        BusinessList.update = update;
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(BusinessList = Weareediblepeople.BusinessList || (Weareediblepeople.BusinessList = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var BusinessPopup;
    (function (BusinessPopup) {
        BusinessPopup.parts = {};
        function _onDOMContentLoaded() {
            BusinessPopup.element = document.getElementById('business-popup');
            setIsUser(!!Weareediblepeople.User.getData());
            BusinessPopup.spinnerHeader = new Weareediblepeople.SpinnerHeader(BusinessPopup.element.querySelector('.review-count'));
            BusinessPopup.parts['close'] = BusinessPopup.element.querySelector('.close');
            BusinessPopup.parts['name'] = BusinessPopup.element.querySelector('.name');
            BusinessPopup.parts['phone'] = BusinessPopup.element.querySelector('.phone');
            BusinessPopup.parts['rating'] = BusinessPopup.element.querySelector('.rating');
            BusinessPopup.parts['tags'] = BusinessPopup.element.querySelector('.tags');
            BusinessPopup.parts['close'].addEventListener('click', hide);
        }
        function show(business) {
            Weareediblepeople.Overlay.show();
            Weareediblepeople.BusinessList.deselectBusiness();
            BusinessPopup.activeId = Weareediblepeople.BusinessList.findBusinessId(business);
            update(business.data);
            populateReviews(BusinessPopup.activeId);
            BusinessPopup.element.classList.add('is-active');
        }
        BusinessPopup.show = show;
        function hide() {
            Weareediblepeople.Overlay.hide();
            BusinessPopup.element.classList.remove('is-active');
        }
        BusinessPopup.hide = hide;
        function setIsUser(isUser) {
            BusinessPopup.element.classList.toggle('is-user', isUser);
        }
        BusinessPopup.setIsUser = setIsUser;
        function update(data) {
            BusinessPopup.parts['name'].textContent = data.name;
            BusinessPopup.parts['phone'].textContent = data.phone;
            BusinessPopup.parts['rating'].style.width = (data.rating * 10) + 'px';
            BusinessPopup.parts['tags'].textContent = data.tags.join(', ');
        }
        BusinessPopup.update = update;
        function _reviewsTitle(num) {
            if (num === 1)
                return '1 review';
            else
                return num + ' reviews';
        }
        function updateCount() {
            BusinessPopup.spinnerHeader.setTitle(_reviewsTitle(Weareediblepeople.ReviewList.reviews.length));
        }
        BusinessPopup.updateCount = updateCount;
        function populateReviews(id) {
            Weareediblepeople.ReviewList.clear();
            updateCount();
            BusinessPopup.spinnerHeader.setActive(true);
            Weareediblepeople.Http.get('/businesses/' + id + '/reviews', function (err, items) {
                if (err === undefined)
                    Weareediblepeople.ReviewList.update(items);
                updateCount();
                BusinessPopup.spinnerHeader.setActive(false);
            });
        }
        BusinessPopup.populateReviews = populateReviews;
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(BusinessPopup = Weareediblepeople.BusinessPopup || (Weareediblepeople.BusinessPopup = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    function _distance(m) {
        if (m >= 300)
            return Math.round((m / 1000) * 10) / 10 + ' km';
        else
            return Math.round(m * 10) / 10 + ' m';
    }
    var Business = (function () {
        function Business(data) {
            var _this = this;
            this._isSelected = false;
            this.parts = {};
            this.buildElement();
            this.update(data);
            this.marker = new google.maps.Marker({
                position: this.data.position,
                title: this.data.name
            });
            this.element.addEventListener('mouseenter', function () {
                Weareediblepeople.BusinessList.toggleBusinessSelected(_this);
            });
            this.marker.addListener('click', function () {
                Weareediblepeople.BusinessPopup.show(_this);
            });
            this.element.addEventListener('click', function () {
                Weareediblepeople.BusinessPopup.show(_this);
            });
        }
        Business.prototype.update = function (data) {
            this.data = data;
            this.parts['name'].textContent = this.data.name;
            this.parts['distance'].textContent = _distance(this.data.distance);
            this.parts['phone'].textContent = this.data.phone;
            this.parts['rating'].style.width = (this.data.rating * 10) + 'px';
            this.parts['tags'].textContent = this.data.tags.join(', ');
        };
        Business.prototype.buildElement = function () {
            this.element = document.createElement('div');
            this.element.classList.add('business', 'business-info');
            this.buildPart('name');
            this.buildPart('distance');
            this.buildPart('phone');
            this.buildPart('rating');
            this.buildPart('tags');
        };
        Business.prototype.buildPart = function (name) {
            this.parts[name] = document.createElement('div');
            this.parts[name].classList.add(name);
            this.element.appendChild(this.parts[name]);
        };
        return Business;
    }());
    Weareediblepeople.Business = Business;
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var LoginForm;
    (function (LoginForm) {
        ;
        function data() {
            return {
                username: LoginForm.fields.username.value,
                password: LoginForm.fields.password.value
            };
        }
        LoginForm.data = data;
        function _onDOMContentLoaded() {
            LoginForm.element = document.getElementById('login-form');
            LoginForm.parts = {
                form: LoginForm.element.querySelector('form'),
                login: LoginForm.element.querySelector('.button-login'),
                signup: LoginForm.element.querySelector('.button-signup')
            };
            LoginForm.fields = {
                username: LoginForm.element.querySelector('input[name="username"]'),
                password: LoginForm.element.querySelector('input[name="password"]'),
            };
            LoginForm.parts.form.addEventListener('submit', _login);
            LoginForm.parts.login.addEventListener('click', _login);
            LoginForm.parts.signup.addEventListener('click', _signup);
            LoginForm.spinnerHeader = new Weareediblepeople.SpinnerHeader(LoginForm.element.querySelector('.spinner-header'));
        }
        function clear() {
            LoginForm.fields.username.value = '';
            LoginForm.fields.password.value = '';
        }
        LoginForm.clear = clear;
        function _login(ev) {
            ev.preventDefault();
            LoginForm.spinnerHeader.setActive(true);
            Weareediblepeople.User.login(data(), function (err) {
                if (err)
                    LoginForm.spinnerHeader.setTitle(err.message);
                else
                    clear();
                LoginForm.spinnerHeader.setActive(false);
            });
        }
        function _signup(ev) {
            ev.preventDefault();
            LoginForm.spinnerHeader.setActive(true);
            Weareediblepeople.User.signup(data(), function (err) {
                if (err)
                    LoginForm.spinnerHeader.setTitle(err.message);
                else
                    clear();
                LoginForm.spinnerHeader.setActive(false);
            });
        }
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(LoginForm = Weareediblepeople.LoginForm || (Weareediblepeople.LoginForm = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var MapWrapper;
    (function (MapWrapper) {
        function _onDOMContentLoaded() {
            MapWrapper.element = document.getElementById('map-wrapper');
        }
        function initMap() {
            if (MapWrapper.map !== undefined)
                throw new Error('Map already initialised.');
            resize(Weareediblepeople.Sidebar.element.offsetWidth);
            MapWrapper.map = new google.maps.Map(document.getElementById('map'), Weareediblepeople.Builders.mapOptions());
            MapWrapper.map.addListener('bounds_changed', Weareediblepeople.Search.perform);
            Weareediblepeople.Geo.getPosition(centerOnPosition);
        }
        MapWrapper.initMap = initMap;
        function centerOnPosition(position) {
            if (MapWrapper.map === undefined)
                throw new Error('Map not initialised.');
            MapWrapper.positionMarker = new google.maps.Marker({
                position: position,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 10
                },
                map: MapWrapper.map
            });
            MapWrapper.map.setCenter(position);
        }
        MapWrapper.centerOnPosition = centerOnPosition;
        function resize(sidebarWidth) {
            MapWrapper.element.style.right = sidebarWidth + 'px';
        }
        MapWrapper.resize = resize;
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(MapWrapper = Weareediblepeople.MapWrapper || (Weareediblepeople.MapWrapper = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var Overlay;
    (function (Overlay) {
        function _onDOMContentLoaded() {
            Overlay.element = document.getElementById('overlay');
            Overlay.element.addEventListener('click', Weareediblepeople.BusinessPopup.hide);
        }
        function show() {
            Overlay.element.classList.add('is-active');
        }
        Overlay.show = show;
        function hide() {
            Overlay.element.classList.remove('is-active');
        }
        Overlay.hide = hide;
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(Overlay = Weareediblepeople.Overlay || (Weareediblepeople.Overlay = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var PostReview;
    (function (PostReview) {
        ;
        function data() {
            return {
                description: PostReview.fields.description.value,
            };
        }
        PostReview.data = data;
        function _onDOMContentLoaded() {
            PostReview.element = document.getElementById('post-review');
            PostReview.parts = {
                form: PostReview.element.querySelector('form'),
                post: PostReview.element.querySelector('.button-post'),
                logout: PostReview.element.querySelector('.button-logout')
            };
            PostReview.fields = {
                description: PostReview.element.querySelector('textarea'),
            };
            PostReview.parts.form.addEventListener('submit', _post);
            PostReview.parts.post.addEventListener('click', _post);
            PostReview.parts.logout.addEventListener('click', _logout);
            PostReview.spinnerHeader = new Weareediblepeople.SpinnerHeader(PostReview.element.querySelector('.spinner-header'));
        }
        function clear() {
            PostReview.fields.description.value = '';
        }
        PostReview.clear = clear;
        function _post(ev) {
            ev.preventDefault();
            PostReview.spinnerHeader.setActive(true);
            Weareediblepeople.Http.post('/businesses/' + Weareediblepeople.BusinessPopup.activeId + '/review', data(), function (err, item) {
                if (err === undefined) {
                    var review = new Weareediblepeople.Review(item);
                    review.setIsUser(true);
                    Weareediblepeople.ReviewList.reviews.unshift(review);
                    Weareediblepeople.ReviewList.element.insertBefore(review.element, Weareediblepeople.ReviewList.element.firstChild);
                    Weareediblepeople.BusinessPopup.updateCount();
                    clear();
                }
                PostReview.spinnerHeader.setActive(false);
            });
        }
        function _logout(ev) {
            ev.preventDefault();
            PostReview.spinnerHeader.setActive(true);
            Weareediblepeople.User.logout(function (err) {
                if (err)
                    PostReview.spinnerHeader.setTitle(err.message);
                else
                    clear();
                PostReview.spinnerHeader.setActive(false);
            });
        }
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(PostReview = Weareediblepeople.PostReview || (Weareediblepeople.PostReview = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var ReviewList;
    (function (ReviewList) {
        ReviewList.reviews = [];
        function _onDOMContentLoaded() {
            ReviewList.element = document.getElementById('review-list');
        }
        function clear() {
            while (ReviewList.element.firstChild) {
                ReviewList.element.removeChild(ReviewList.element.firstChild);
            }
            ReviewList.reviews = [];
        }
        ReviewList.clear = clear;
        function setIsUser(username) {
            for (var _i = 0, reviews_1 = ReviewList.reviews; _i < reviews_1.length; _i++) {
                var review = reviews_1[_i];
                ReviewList.element.appendChild(review.element);
                review.setIsUser(username !== undefined && review.data.username === username);
            }
        }
        ReviewList.setIsUser = setIsUser;
        function update(items) {
            clear();
            ReviewList.reviews = items.map(function (item) { return new Weareediblepeople.Review(item); });
            for (var _i = 0, reviews_2 = ReviewList.reviews; _i < reviews_2.length; _i++) {
                var review = reviews_2[_i];
                ReviewList.element.appendChild(review.element);
            }
            var user = Weareediblepeople.User.getData();
            setIsUser(user ? user.username : undefined);
        }
        ReviewList.update = update;
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(ReviewList = Weareediblepeople.ReviewList || (Weareediblepeople.ReviewList = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    function _description(str) {
        str = str.replace(/&/g, "&amp;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/'/g, "&#039;");
        str = str.replace('\n\n', '</p><p>');
        str = str.replace('\n', '<br />');
        return '<p>' + str + '</p>';
    }
    var Review = (function () {
        function Review(data) {
            this.parts = {};
            this.buildElement();
            this.update(data);
            this.parts['trash'].addEventListener('click', this.destroy.bind(this));
        }
        Review.prototype.destroy = function () {
            var _this = this;
            Weareediblepeople.Http.destroy('/reviews/' + this.data.id, function (err) {
                var index = Weareediblepeople.ReviewList.reviews.indexOf(_this);
                if (index > -1)
                    Weareediblepeople.ReviewList.reviews.splice(index, 1);
                Weareediblepeople.BusinessPopup.updateCount();
                Weareediblepeople.ReviewList.element.removeChild(_this.element);
            });
        };
        Review.prototype.update = function (data) {
            this.data = data;
            this.parts['username'].textContent = this.data.username;
            this.parts['created-at'].textContent = this.data.createdAt;
            this.parts['description'].innerHTML = _description(this.data.description);
        };
        Review.prototype.buildElement = function () {
            this.element = document.createElement('div');
            this.element.classList.add('review');
            this.buildPart('trash');
            this.parts['trash'].textContent = '✖';
            this.buildPart('username');
            this.buildPart('created-at');
            this.buildPart('description');
        };
        Review.prototype.setIsUser = function (isUser) {
            this.element.classList.toggle('is-user', isUser);
        };
        Review.prototype.buildPart = function (name) {
            this.parts[name] = document.createElement('div');
            this.parts[name].classList.add(name);
            this.element.appendChild(this.parts[name]);
        };
        return Review;
    }());
    Weareediblepeople.Review = Review;
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var Search;
    (function (Search) {
        Search.isActive = false;
        var _perform;
        function _onDOMContentLoaded() {
            Search.element = document.getElementById('search');
            var searchBar = document.getElementById('search-bar');
            Search.parts = {
                icon: document.getElementById('search-icon'),
                bar: searchBar,
                form: searchBar.querySelector('form'),
                input: searchBar.querySelector('input')
            };
            Search.parts.icon.addEventListener('click', focus);
            Search.parts.form.addEventListener('submit', _onSubmit);
            Search.parts.input.addEventListener('blur', blur);
        }
        function _onSubmit(ev) {
            ev.preventDefault();
            perform();
            blur();
        }
        function perform() {
            _perform = _perform || Weareediblepeople.debounce(function () {
                return Weareediblepeople.Query.businesses(Weareediblepeople.BusinessList.update);
            }, 250);
            _perform();
        }
        Search.perform = perform;
        function getValue() {
            return Search.parts.input.value || '';
        }
        Search.getValue = getValue;
        function focus() {
            Search.element.classList.add('is-active');
            Search.parts.input.select();
        }
        Search.focus = focus;
        function blur() {
            Search.element.classList.remove('is-active');
        }
        Search.blur = blur;
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(Search = Weareediblepeople.Search || (Weareediblepeople.Search = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var Sidebar;
    (function (Sidebar) {
        function _onDOMContentLoaded() {
            Sidebar.element = document.getElementById('sidebar');
            Sidebar.spinnerHeader = new Weareediblepeople.SpinnerHeader(Sidebar.element.querySelector('.spinner-header'));
        }
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(Sidebar = Weareediblepeople.Sidebar || (Weareediblepeople.Sidebar = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var SpinnerHeader = (function () {
        function SpinnerHeader(element) {
            this._activeCount = 0;
            this.element = element;
            this.title = element.querySelector('.title');
            this._defaultTitle = this.title.textContent;
        }
        SpinnerHeader.prototype.setActive = function (isActive) {
            this._activeCount += (isActive ? 1 : -1);
            if (this._activeCount > 0)
                isActive = true;
            this.element.classList.toggle('is-active', isActive);
        };
        SpinnerHeader.prototype.setTitle = function (title) {
            if (title === void 0) { title = this._defaultTitle; }
            this.title.textContent = title;
        };
        return SpinnerHeader;
    }());
    Weareediblepeople.SpinnerHeader = SpinnerHeader;
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var Warning;
    (function (Warning) {
        function _onDOMContentLoaded() {
            Warning.element = document.getElementById('warning');
        }
        var _timeout;
        function show(message) {
            Warning.element.textContent = message;
            Warning.element.classList.add('is-active');
            clearTimeout(_timeout);
            _timeout = setTimeout(function () {
                Warning.element.classList.remove('is-active');
            }, 3000);
        }
        Warning.show = show;
        window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
    })(Warning = Weareediblepeople.Warning || (Weareediblepeople.Warning = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var Builders;
    (function (Builders) {
        function mapOptions() {
            return {
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                styles: [{
                        featureType: 'poi.business',
                        stylers: [{ visibility: 'off' }]
                    }]
            };
        }
        Builders.mapOptions = mapOptions;
        function qs(params) {
            var parts = [];
            for (var _i = 0, _a = Object.keys(params); _i < _a.length; _i++) {
                var key = _a[_i];
                parts.push(encodeURIComponent(key) + "=" + encodeURIComponent(params[key]));
            }
            return parts.join('&');
        }
        Builders.qs = qs;
    })(Builders = Weareediblepeople.Builders || (Weareediblepeople.Builders = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    function debounce(func, wait, immediate) {
        if (immediate === void 0) { immediate = false; }
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate)
                    func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow)
                func.apply(context, args);
        };
    }
    Weareediblepeople.debounce = debounce;
    ;
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var Geo;
    (function (Geo) {
        var POSITIONS = {
            tokyo: { lat: 35.640278, lng: 139.694763 },
            toronto: { lat: 43.761539, lng: -79.411079 },
            melbourne: { lat: -37.815018, lng: 144.946014 },
            paris: { lat: 48.870502, lng: 2.304897 }
        };
        function _randomPosition() {
            var keys = Object.keys(POSITIONS);
            var key = keys[Math.floor(Math.random() * keys.length)];
            return POSITIONS[key];
        }
        function getPosition(callback) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    callback({
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    });
                }, function () {
                    Weareediblepeople.Warning.show('Warning: Geolocation service failed, using random location.');
                    callback(_randomPosition());
                });
            }
            else {
                Weareediblepeople.Warning.show('Warning: Browser doesn\'t support geolocation, using random location.');
                callback(_randomPosition());
            }
        }
        Geo.getPosition = getPosition;
    })(Geo = Weareediblepeople.Geo || (Weareediblepeople.Geo = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var Http;
    (function (Http) {
        var _handleResponse = function (xhr, callback) {
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    var response = JSON.parse(xhr.responseText);
                    if (xhr.status >= 200 && xhr.status < 400)
                        callback(undefined, response);
                    else {
                        var err = new Error(response.error.message);
                        err.name = response.error.name;
                        callback(err, response);
                    }
                }
            };
        };
        function post(url, data, callback) {
            var xhr = new XMLHttpRequest();
            var qs = Weareediblepeople.Builders.qs(data);
            xhr.open('POST', url, true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send(qs);
            _handleResponse(xhr, callback);
        }
        Http.post = post;
        function get(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send();
            _handleResponse(xhr, callback);
        }
        Http.get = get;
        function destroy(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.open('DELETE', url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.send();
            _handleResponse(xhr, callback);
        }
        Http.destroy = destroy;
    })(Http = Weareediblepeople.Http || (Weareediblepeople.Http = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var Query;
    (function (Query) {
        function _mapRadius() {
            var bounds = Weareediblepeople.MapWrapper.map.getBounds();
            var ne = bounds.getNorthEast();
            var sw = bounds.getSouthWest();
            return google.maps.geometry.spherical.computeDistanceBetween(ne, sw) / 2;
        }
        function _capitalise(text) {
            return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
        }
        function businesses(callback) {
            if (Weareediblepeople.MapWrapper.map === undefined)
                throw new Error('Map not initialised.');
            var term = Weareediblepeople.Search.getValue();
            Weareediblepeople.Sidebar.spinnerHeader.setActive(true);
            Weareediblepeople.Sidebar.spinnerHeader.setTitle('Nearby ' + (_capitalise(term) || 'edibles'));
            var position = Weareediblepeople.MapWrapper.map.getCenter();
            var qs = Weareediblepeople.Builders.qs({
                lat: position.lat(),
                lng: position.lng(),
                radius: _mapRadius(),
                term: term
            });
            Weareediblepeople.Http.get('/businesses?' + qs, function (err, results) {
                if (err === undefined)
                    callback(results);
                Weareediblepeople.Sidebar.spinnerHeader.setActive(false);
            });
        }
        Query.businesses = businesses;
    })(Query = Weareediblepeople.Query || (Weareediblepeople.Query = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
var Weareediblepeople;
(function (Weareediblepeople) {
    var User;
    (function (User) {
        function _getCookieValue(name) {
            var value = document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)');
            return value ? value.pop() : '';
        }
        function getData() {
            var jwt = _getCookieValue('userData');
            var result;
            try {
                result = jwt_decode(jwt);
            }
            catch (e) { }
            return result;
        }
        User.getData = getData;
        function signup(data, callback) {
            Weareediblepeople.Http.post('/users', data, function (err) {
                if (err === undefined) {
                    Weareediblepeople.BusinessPopup.setIsUser(true);
                    Weareediblepeople.ReviewList.setIsUser(data.username);
                }
                callback(err);
            });
        }
        User.signup = signup;
        function login(data, callback) {
            Weareediblepeople.Http.post('/users/login', data, function (err) {
                if (err === undefined) {
                    Weareediblepeople.BusinessPopup.setIsUser(true);
                    Weareediblepeople.ReviewList.setIsUser(data.username);
                }
                callback(err);
            });
        }
        User.login = login;
        function logout(callback) {
            Weareediblepeople.Http.get('/users/logout', function (err) {
                if (err === undefined) {
                    Weareediblepeople.BusinessPopup.setIsUser(false);
                    Weareediblepeople.ReviewList.setIsUser(undefined);
                }
                callback(err);
            });
        }
        User.logout = logout;
    })(User = Weareediblepeople.User || (Weareediblepeople.User = {}));
})(Weareediblepeople || (Weareediblepeople = {}));
//# sourceMappingURL=main.js.map