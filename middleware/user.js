'use strict';
const jwt = require('jwt-simple');
const User = require('../models/user');

function attachUser(id, res, next) {
    if (!id)
        next();
    else {
        User.read(id, (err, user) => {
            if (err)
                res.clearCookie('userData');
            else
                res.locals.user = user;
            next();
        });
    }
}

module.exports = (req, res, next) => {
    const token = req.cookies['userData'];
    if (!token)
        next();
    else {
        let decoded;
        try { decoded = jwt.decode(token, process.env.JWT_SECRET); }
        catch (e) {}
        attachUser((decoded || {})['id'], res, next);
    }
}
