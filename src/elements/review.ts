namespace Weareediblepeople {
    function _description(str: string): string {
        str = str.replace(/&/g, "&amp;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/'/g, "&#039;");
        str = str.replace('\n\n', '</p><p>');
        str = str.replace('\n', '<br />');
        return '<p>' + str + '</p>';
    }

    export interface ReviewData {
        id: string;
        username: string;
        createdAt: string;
        description: string;
    }

    export class Review {
        data: ReviewData;
        element: HTMLElement;
        parts: { [index: string]: HTMLElement } = {};

        constructor(data: ReviewData) {
            this.buildElement();
            this.update(data);
            this.parts['trash'].addEventListener('click', this.destroy.bind(this));
        }

        destroy() {
            Http.destroy('/reviews/' + this.data.id, (err: Error) => {
                const index = ReviewList.reviews.indexOf(this);
                if (index > -1)
                    ReviewList.reviews.splice(index, 1);
                BusinessPopup.updateCount();
                ReviewList.element.removeChild(this.element);
            });
        }

        update(data: ReviewData) {
            this.data = data;
            this.parts['username'].textContent = this.data.username;
            this.parts['created-at'].textContent = this.data.createdAt;
            this.parts['description'].innerHTML = _description(this.data.description);
        }

        buildElement() {
            this.element = document.createElement('div');
            this.element.classList.add('review');
            this.buildPart('trash');
            this.parts['trash'].textContent = '✖';
            this.buildPart('username');
            this.buildPart('created-at');
            this.buildPart('description');
        }

        setIsUser(isUser: boolean) {
            this.element.classList.toggle('is-user', isUser);
        }

        buildPart(name: string) {
            this.parts[name] = document.createElement('div');
            this.parts[name].classList.add(name);
            this.element.appendChild(this.parts[name]);
        }
    }
}
