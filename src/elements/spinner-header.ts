namespace Weareediblepeople {
    export class SpinnerHeader {
        private _activeCount: number = 0;
        private _defaultTitle: string;

        element: HTMLElement;
        title: HTMLElement;

        constructor(element: HTMLElement) {
            this.element = element;
            this.title = <HTMLElement>element.querySelector('.title');
            this._defaultTitle = this.title.textContent;
        }

        setActive(isActive: boolean) {
            this._activeCount += (isActive ? 1 : -1);
            if (this._activeCount > 0) isActive = true;
            this.element.classList.toggle('is-active', isActive);
        }

        setTitle(title: string = this._defaultTitle) {
            this.title.textContent = title;
        }
    }
}
