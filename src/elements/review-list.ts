namespace Weareediblepeople.ReviewList {
    export let element: HTMLElement;
    export let reviews: Review[] = [];

    function _onDOMContentLoaded() {
        element = document.getElementById('review-list');
    }

    export function clear() {
        while (element.firstChild) {
            element.removeChild(element.firstChild);
        }
        reviews = [];
    }

    export function setIsUser(username: string) {
        for (const review of reviews) {
            element.appendChild(review.element);
            review.setIsUser(username !== undefined && review.data.username === username);
        }
    }

    export function update(items: ReviewData[]) {
        clear();
        reviews = items.map(item => new Review(item));
        for (const review of reviews) {
            element.appendChild(review.element);
        }
        const user = User.getData();
        setIsUser(user ? user.username : undefined);
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
