namespace Weareediblepeople.Warning {
    export let element: HTMLElement;

    function _onDOMContentLoaded() {
        element = document.getElementById('warning');
    }

    let _timeout: number;

    export function show(message: string) {
        element.textContent = message;
        element.classList.add('is-active');
        clearTimeout(_timeout);
        _timeout = setTimeout(() => {
            element.classList.remove('is-active');
        }, 3000);
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
