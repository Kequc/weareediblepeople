namespace Weareediblepeople.Sidebar {
    export let element: HTMLElement;
    export let spinnerHeader: SpinnerHeader;

    function _onDOMContentLoaded() {
        element = document.getElementById('sidebar');
        spinnerHeader = new SpinnerHeader(<HTMLElement>element.querySelector('.spinner-header'))
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
