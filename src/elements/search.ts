namespace Weareediblepeople.Search {
    export let element: HTMLElement;
    export let parts: {
        icon: HTMLElement;
        bar: HTMLElement;
        form: HTMLFormElement;
        input: HTMLInputElement;
    };
    export let isActive: boolean = false;

    let _perform: Function;

    function _onDOMContentLoaded() {
        element = document.getElementById('search');
        const searchBar = document.getElementById('search-bar');
        parts = {
            icon: document.getElementById('search-icon'),
            bar: searchBar,
            form: searchBar.querySelector('form'),
            input: searchBar.querySelector('input')
        };
        parts.icon.addEventListener('click', focus);
        parts.form.addEventListener('submit', _onSubmit);
        parts.input.addEventListener('blur', blur);
    }

    function _onSubmit(ev: Event) {
        ev.preventDefault();
        perform();
        blur();
    }

    export function perform() {
        _perform = _perform || debounce(() =>
            Query.businesses(BusinessList.update),
        250);
        _perform();
    }

    export function getValue(): string {
        return parts.input.value || '';
    }

    export function focus() {
        element.classList.add('is-active');
        parts.input.select();
    }

    export function blur() {
        element.classList.remove('is-active');
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
