namespace Weareediblepeople.LoginForm {
    export let element: HTMLElement;
    export let parts: {
        form: HTMLFormElement;
        login: HTMLInputElement;
        signup: HTMLInputElement;
    };
    export let fields: {
        username: HTMLInputElement;
        password: HTMLInputElement;
    };
    export let spinnerHeader: SpinnerHeader;

    export interface Data {
        username: string;
        password: string;
    };

    export function data(): Data {
        return {
            username: fields.username.value,
            password: fields.password.value
        };
    }

    function _onDOMContentLoaded() {
        element = document.getElementById('login-form');
        parts = {
            form: element.querySelector('form'),
            login: <HTMLInputElement>element.querySelector('.button-login'),
            signup: <HTMLInputElement>element.querySelector('.button-signup')
        };
        fields = {
            username: <HTMLInputElement>element.querySelector('input[name="username"]'),
            password: <HTMLInputElement>element.querySelector('input[name="password"]'),
        };
        parts.form.addEventListener('submit', _login);
        parts.login.addEventListener('click', _login);
        parts.signup.addEventListener('click', _signup);
        spinnerHeader = new SpinnerHeader(<HTMLElement>element.querySelector('.spinner-header'));
    }

    export function clear() {
        fields.username.value = '';
        fields.password.value = '';
    }

    function _login(ev: Event) {
        ev.preventDefault();
        spinnerHeader.setActive(true);
        User.login(data(), (err: Error) => {
            if (err)
                spinnerHeader.setTitle(err.message);
            else
                clear();
            spinnerHeader.setActive(false);
        });
    }

    function _signup(ev: Event) {
        ev.preventDefault();
        spinnerHeader.setActive(true);
        User.signup(data(), (err: Error) => {
            if (err)
                spinnerHeader.setTitle(err.message);
            else
                clear();
            spinnerHeader.setActive(false);
        });
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
