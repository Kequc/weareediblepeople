namespace Weareediblepeople.BusinessPopup {
    export let element: HTMLElement;
    export let spinnerHeader: SpinnerHeader;
    export const parts: { [index: string]: HTMLElement } = {};
    export let activeId: string;

    function _onDOMContentLoaded() {
        element = document.getElementById('business-popup');
        setIsUser(!!User.getData());
        spinnerHeader = new SpinnerHeader(<HTMLElement>element.querySelector('.review-count'));
        parts['close'] = <HTMLElement>element.querySelector('.close');
        parts['name'] = <HTMLElement>element.querySelector('.name');
        parts['phone'] = <HTMLElement>element.querySelector('.phone');
        parts['rating'] = <HTMLElement>element.querySelector('.rating');
        parts['tags'] = <HTMLElement>element.querySelector('.tags');
        parts['close'].addEventListener('click', hide);
    }

    export function show(business: Business) {
        Overlay.show();
        BusinessList.deselectBusiness();
        activeId = BusinessList.findBusinessId(business);
        update(business.data);
        populateReviews(activeId);
        element.classList.add('is-active');
    }

    export function hide() {
        Overlay.hide();
        element.classList.remove('is-active');
    }

    export function setIsUser(isUser: boolean) {
        element.classList.toggle('is-user', isUser);
    }

    export function update(data: BusinessData) {
        parts['name'].textContent = data.name;
        parts['phone'].textContent = data.phone;
        parts['rating'].style.width = (data.rating * 10) + 'px';
        parts['tags'].textContent = data.tags.join(', ');
    }

    function _reviewsTitle(num: number) {
        if (num === 1) return '1 review';
        else return num + ' reviews';
    }

    export function updateCount() {
        spinnerHeader.setTitle(_reviewsTitle(ReviewList.reviews.length));
    }

    export function populateReviews(id: string) {
        ReviewList.clear();
        updateCount();
        spinnerHeader.setActive(true);
        Http.get('/businesses/' + id + '/reviews', (err: Error, items: ReviewData[]) => {
            if (err === undefined)
                ReviewList.update(items);
            updateCount();
            spinnerHeader.setActive(false);
        });
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
