namespace Weareediblepeople.PostReview {
    export let element: HTMLElement;
    export let parts: {
        form: HTMLFormElement;
        post: HTMLInputElement;
        logout: HTMLInputElement;
    };
    export let fields: {
        description: HTMLTextAreaElement;
    };
    export let spinnerHeader: SpinnerHeader;

    export interface Data {
        description: string;
    };

    export function data(): Data {
        return {
            description: fields.description.value,
        };
    }

    function _onDOMContentLoaded() {
        element = document.getElementById('post-review');
        parts = {
            form: element.querySelector('form'),
            post: <HTMLInputElement>element.querySelector('.button-post'),
            logout: <HTMLInputElement>element.querySelector('.button-logout')
        };
        fields = {
            description: element.querySelector('textarea'),
        };
        parts.form.addEventListener('submit', _post);
        parts.post.addEventListener('click', _post);
        parts.logout.addEventListener('click', _logout);
        spinnerHeader = new SpinnerHeader(<HTMLElement>element.querySelector('.spinner-header'));
    }

    export function clear() {
        fields.description.value = '';
    }

    function _post(ev: Event) {
        ev.preventDefault();
        spinnerHeader.setActive(true);
        Http.post('/businesses/' + BusinessPopup.activeId + '/review', data(), (err: Error, item: ReviewData) => {
            if (err === undefined) {
                const review = new Review(item);
                review.setIsUser(true);
                ReviewList.reviews.unshift(review);
                ReviewList.element.insertBefore(review.element, ReviewList.element.firstChild);
                BusinessPopup.updateCount();
                clear();
            }
            spinnerHeader.setActive(false);
        });
    }

    function _logout(ev: Event) {
        ev.preventDefault();
        spinnerHeader.setActive(true);
        User.logout((err: Error) => {
            if (err)
                spinnerHeader.setTitle(err.message);
            else
                clear();
            spinnerHeader.setActive(false);
        });
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
