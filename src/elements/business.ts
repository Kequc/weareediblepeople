namespace Weareediblepeople {
    function _distance(m: number): string {
        if (m >= 300)
            return Math.round((m / 1000) * 10) / 10 + ' km'
        else
            return Math.round(m * 10) / 10 + ' m';
    }

    export interface BusinessData {
        name: string;
        distance: number;
        phone: string;
        rating: number;
        position: { lat: number, lng: number };
        tags: string[];
    }

    export class Business {
        private _isSelected: boolean = false;

        data: BusinessData;
        element: HTMLElement;
        parts: { [index: string]: HTMLElement } = {};
        marker: google.maps.Marker;

        constructor(data: BusinessData) {
            this.buildElement();
            this.update(data);
            this.marker = new google.maps.Marker({
                position: this.data.position,
                title: this.data.name
            });
            this.element.addEventListener('mouseenter', () => {
                BusinessList.toggleBusinessSelected(this);
            });
            this.marker.addListener('click', () => {
                BusinessPopup.show(this);
            });
            this.element.addEventListener('click', () => {
                BusinessPopup.show(this);
            });
        }

        update(data: BusinessData) {
            this.data = data;
            this.parts['name'].textContent = this.data.name;
            this.parts['distance'].textContent = _distance(this.data.distance);
            this.parts['phone'].textContent = this.data.phone;
            this.parts['rating'].style.width = (this.data.rating * 10) + 'px';
            this.parts['tags'].textContent = this.data.tags.join(', ');
        }

        buildElement() {
            this.element = document.createElement('div');
            this.element.classList.add('business', 'business-info');
            this.buildPart('name');
            this.buildPart('distance');
            this.buildPart('phone');
            this.buildPart('rating');
            this.buildPart('tags');
        }

        buildPart(name: string) {
            this.parts[name] = document.createElement('div');
            this.parts[name].classList.add(name);
            this.element.appendChild(this.parts[name]);
        }
    }
}
