namespace Weareediblepeople.Overlay {
    export let element: HTMLElement;

    function _onDOMContentLoaded() {
        element = document.getElementById('overlay');
        element.addEventListener('click', BusinessPopup.hide);
    }

    export function show() {
        element.classList.add('is-active');
    }

    export function hide() {
        element.classList.remove('is-active');
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
