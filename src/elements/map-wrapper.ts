namespace Weareediblepeople.MapWrapper {
    export let element: HTMLElement;

    function _onDOMContentLoaded() {
        element = document.getElementById('map-wrapper');
    }

    // Google Maps API reference
    // https://developers.google.com/maps/documentation/javascript/adding-a-google-map

    export let map: google.maps.Map;
    export let positionMarker: google.maps.Marker;

    export function initMap() {
        if (map !== undefined)
            throw new Error('Map already initialised.');

        resize(Sidebar.element.offsetWidth);

        map = new google.maps.Map(document.getElementById('map'), Builders.mapOptions());
        map.addListener('bounds_changed', Search.perform);

        Geo.getPosition(centerOnPosition);
    }

    export function centerOnPosition(position: google.maps.LatLngLiteral) {
        if (map === undefined)
            throw new Error('Map not initialised.');

        positionMarker = new google.maps.Marker({
            position: position,
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 10
            },
            map
        });

        map.setCenter(position);
    }

    export function resize(sidebarWidth: number) {
        element.style.right = sidebarWidth + 'px';
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
