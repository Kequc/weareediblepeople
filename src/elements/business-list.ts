namespace Weareediblepeople.BusinessList {
    export let element: HTMLElement;

    function _onDOMContentLoaded() {
        element = document.getElementById('business-list');
        element.addEventListener('mouseleave', deselectBusiness);
    }

    let _businesses: { [id: string]: Business } = {};
    let _visible: string[] = [];
    let _selected: string;

    export function findBusiness(id: string): Business {
        return _businesses[id];
    }

    export function findBusinessId(business: Business): string {
        for (const id of Object.keys(_businesses)) {
            if (_businesses[id] === business) return id;
        }
    }

    export function toggleBusinessSelected(business: Business) {
        const id = findBusinessId(business);
        if (_selected !== id)
            selectBusiness(id);
        else
            deselectBusiness();
    }

    export function selectBusiness(id: string) {
        deselectBusiness();
        _businesses[id].marker.setAnimation(google.maps.Animation.BOUNCE);
        _selected = id;
    }

    export function deselectBusiness() {
        if (_selected === undefined) return;
        _businesses[_selected].marker.setAnimation(null);
        _selected = undefined;
    }

    function _storeBusinesses(items: { [id: string]: BusinessData }) {
        for (const id of Object.keys(items)) {
            const business = _businesses[id];
            if (business !== undefined)
                business.update(items[id]);
            else
                _businesses[id] = new Business(items[id]);
        }
    }

    function _forViewport(id: string): boolean {
        const bounds = MapWrapper.map.getBounds();
        return bounds.contains(_businesses[id].data.position);
    }

    function _render(ids: string[]) {
        ids = ids.filter(_forViewport);
        for (const id of _visible) {
            if (ids.indexOf(id) <= -1) {
                element.removeChild(_businesses[id].element);
                _businesses[id].marker.setMap(null);
            }
        }
        for (const id of ids) {
            element.appendChild(_businesses[id].element);
            if (_visible.indexOf(id) <= -1)
                _businesses[id].marker.setMap(MapWrapper.map);
        }
        if (ids.indexOf(_selected) <= -1) deselectBusiness();
        _visible = ids;
    }

    export function update(items: { [id: string]: BusinessData }) {
        _storeBusinesses(items);
        const ids = Object.keys(items);
        _render(ids);
    }

    window.addEventListener('DOMContentLoaded', _onDOMContentLoaded);
}
