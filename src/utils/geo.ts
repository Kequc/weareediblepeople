namespace Weareediblepeople.Geo {
    const POSITIONS: { [index: string]: google.maps.LatLngLiteral } = {
        tokyo: { lat: 35.640278, lng: 139.694763 },
        toronto: { lat: 43.761539, lng: -79.411079 },
        melbourne: { lat: -37.815018, lng: 144.946014 },
        paris: { lat: 48.870502, lng: 2.304897 }
    };

    function _randomPosition(): google.maps.LatLngLiteral {
        const keys = Object.keys(POSITIONS);
        const key = keys[Math.floor(Math.random() * keys.length)];
        return POSITIONS[key];
    }

    export function getPosition(callback: (pos: google.maps.LatLngLiteral) => any) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                callback({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                });
            }, () => {
                Warning.show('Warning: Geolocation service failed, using random location.');
                callback(_randomPosition());
            });
        } else {
            Warning.show('Warning: Browser doesn\'t support geolocation, using random location.');
            callback(_randomPosition());
        }
    }
}
