namespace Weareediblepeople.Builders {
    export function mapOptions(): google.maps.MapOptions {
        return {
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            styles: [{
                featureType: 'poi.business',
                stylers: [{ visibility: 'off' }]
            }]
        };
    }

    export function qs(params: { [index: string]: any }): string {
        const parts = [];
        for (const key of Object.keys(params)) {
            parts.push(encodeURIComponent(key) + "=" + encodeURIComponent(params[key]));
        }
        return parts.join('&');
    }
}
