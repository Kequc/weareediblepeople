namespace Weareediblepeople.Query {
    function _mapRadius() {
        const bounds = MapWrapper.map.getBounds();
        const ne = bounds.getNorthEast();
        const sw = bounds.getSouthWest();
        return google.maps.geometry.spherical.computeDistanceBetween(ne, sw) / 2;
    }

    function _capitalise(text: string): string {
        return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
    }

    export function businesses(callback: (results: { [id: string]: BusinessData }) => any) {
        if (MapWrapper.map === undefined)
            throw new Error('Map not initialised.');

        const term = Search.getValue();

        Sidebar.spinnerHeader.setActive(true);
        Sidebar.spinnerHeader.setTitle('Nearby ' + (_capitalise(term) || 'edibles'));

        const position = MapWrapper.map.getCenter();
        const qs = Builders.qs({
            lat: position.lat(),
            lng: position.lng(),
            radius: _mapRadius(),
            term
        });

        Http.get('/businesses?' + qs, (err: Error, results: { [id: string]: BusinessData }) => {
            if (err === undefined)
                callback(results);
            Sidebar.spinnerHeader.setActive(false);
        });
    }
}
