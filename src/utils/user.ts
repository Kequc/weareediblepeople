namespace Weareediblepeople.User {
    declare function jwt_decode(jwt: string) : any;

    function _getCookieValue(name: string): string {
        const value = document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)');
        return value ? value.pop() : '';
    }

    export interface UserData {
        id: string;
        username: string;
    }

    export function getData(): UserData {
        const jwt = _getCookieValue('userData');
        let result;
        try { result = jwt_decode(jwt); }
        catch (e) {}
        return result; 
    }

    export function signup(data: LoginForm.Data, callback: (err?: Error) => any) {
        Http.post('/users', data, (err: Error) => {
            if (err === undefined) {
                BusinessPopup.setIsUser(true);
                ReviewList.setIsUser(data.username);
            }
            callback(err);
        });
    }

    export function login(data: LoginForm.Data, callback: (err?: Error) => any) {
        Http.post('/users/login', data, (err: Error) => {
            if (err === undefined) {
                BusinessPopup.setIsUser(true);
                ReviewList.setIsUser(data.username);
            }
            callback(err);
        });
    }

    export function logout(callback: (err?: Error) => any) {
        Http.get('/users/logout', (err: Error) => {
            if (err === undefined) {
                BusinessPopup.setIsUser(false);
                ReviewList.setIsUser(undefined);
            }
            callback(err);
        });
    }
}
