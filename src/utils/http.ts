namespace Weareediblepeople.Http {
    const _handleResponse = (xhr: XMLHttpRequest, callback: (err: Error, response?: any) => any) => {
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                const response = JSON.parse(xhr.responseText);
                if (xhr.status >= 200 && xhr.status < 400)
                    callback(undefined, response);
                else {
                    const err = new Error(response.error.message);
                    err.name = response.error.name;
                    callback(err, response);
                }
            }
        };
    };

    export function post(url: string, data: any, callback: (err: Error, response?: any) => any) {
        const xhr = new XMLHttpRequest();
        const qs = Builders.qs(data);
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send(qs);
        _handleResponse(xhr, callback);
    }

    export function get(url: string, callback: (err: Error, response?: any) => any) {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send();
        _handleResponse(xhr, callback);
    }

    export function destroy(url: string, callback: (err: Error, response?: any) => any) {
        const xhr = new XMLHttpRequest();
        xhr.open('DELETE', url, true);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send();
        _handleResponse(xhr, callback);
    }
}
