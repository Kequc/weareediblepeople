'use strict';
const { Model, DocOperations, FindOperations } = require('couch-recliner');
const bcrypt = require('bcrypt');
const couch = require('./couch');

class User extends Model {
    static create(body, callback) {
        User.findByUsername(body['username'], (err, doc) => {
            if (err && err.name === 'not_found') {
                bcrypt.hash(body['password'], 10, (err, passwordHash) => {
                    if (err)
                        callback(err);
                    else {
                        const clone = Object.assign({}, body, { passwordHash });
                        delete clone['password'];
                        DocOperations.create(this, clone, callback);
                    }
                });
            }
            else if (err)
                callback(err);
            else {
                const err = new Error('Username is in use.');
                err.status = 400;
                callback(err);
            }
        });
    }

    static authenticate(username, password, callback) {
        const AUTH_ERR = new Error('Authentication failed.');
        AUTH_ERR.status = 401;

        User.findByUsername(username, (err, doc) => {
            if (err)
                callback(AUTH_ERR);
            else {
                bcrypt.compare(password, doc.body['passwordHash'], (err, success) => {
                    if (err || !success)
                        callback(AUTH_ERR);
                    else
                        callback(undefined, doc);
                });
            }
        });
    }

    static findByUsername(username, callback) {
        const finder = {
            selector: { username },
        };
        FindOperations.findOne(this, finder, callback);
    }

    toJson() {
        return Object.assign({ id: this.id, username: this.body.username });
    }
}
 
User.dbName = 'users';
User.couch = couch;

module.exports = User;
