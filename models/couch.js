'use strict';
const { Couch } = require('couch-recliner');
const Vcap = require('../utils/vcap');

const cred = Vcap.serviceCredentials('cloudantNoSQLDB') || process.env.COUCH;
const couch = new Couch({
    any: (cred !== undefined ? cred.url : 'http://localhost:5984')
});

module.exports = couch;
