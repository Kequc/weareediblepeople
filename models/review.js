'use strict';
const moment = require('moment');
const { Model, FindOperations } = require('couch-recliner');
const couch = require('./couch');
 
class Review extends Model {
    static findByBusinessId(businessId, callback) {
        const finder = {
            selector: { businessId },
            limit: 50
        };
        FindOperations.find(this, finder, callback);
    }

    toJson() {
        const createdAt = moment(Date.now()).to(this.body.createdAt);
        return Object.assign({ id: this.id }, this.body, { createdAt });
    }
}
 
Review.dbName = 'reviews';
Review.couch = couch;

module.exports = Review;
